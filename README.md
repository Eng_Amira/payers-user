# Prerequisites
* Ruby version:    2.5.1
* Rails Version:   5.2.1
* Used Gems:

  gem 'clearance' Used for Authentication

  gem 'mini_magick'  Used for ActiveStorage variant

  gem 'device_detector' Used to detect devise details

  gem 'maxminddb' Used to read IP details

  gem 'active_model_otp' Used to implement Google's MFA authenticator

  gem 'rqrcode' Used to implement QR Code

  gem 'jwt' Used for Authentice user via token

  gem 'rspec-rails' ~> 3.5  Test

  gem 'shoulda-matchers' ~> 3.1 Test 

  gem 'factory_bot_rails' ~> 4.0 Test

  gem 'faker' Test
  
  gem 'database_cleaner' Test

# Main Programming Parts
* Sign Up

 Users sign up using Valid email, username and password. 

 API documentation link:  https://payers-userpanel.herokuapp.com/api/index.html#api-User-postUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#create-instance_method

* Confirm user mail after registeration.

 API documentation link:  https://payers-userpanel.herokuapp.com/api/index.html#api-User-POSTconfirmmail

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#confirmmail-instance_method

* Sign In

 Users can sign in using Valid email and password.

 API documentation link:  https://payers-userpanel.herokuapp.com/api/index.html#api-Authentication-AuthenticateUser

 Code documentation Link: http://0.0.0.0:8808/docs/Clearance/SessionsController#create-instance_method

* confirm sign in via two factor authentication

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#get_two_factor-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#send_confirmation_email-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#post_two_factor-instance_method

* Send email to Reset Password.

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/PasswordsController#create-instance_method

* Reset User's Password.

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/PasswordsController#update-instance_method

* UnLock User account

 UnLock account after Number of failed attempts.

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Authentication-unlockaccount

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#unlockaccount-instance_method

* Show user profile 

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-GetUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#show-instance_method

* Send Invitation Email

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-AffilateProgram-postAffilateProgram

* Edit user profile

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-PutUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#edit-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#update-instance_method

* Choose another two factor authentication 

Code documentation Link: http://0.0.0.0:8808/docs/UsersController#change_confirmation_code-instance_method

Code documentation Link: http://0.0.0.0:8808/docs/UsersController#confirm_google_code-instance_method

* Show User Log

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Log-Getuserlog

 Code documentation Link: http://0.0.0.0:8808/docs/WatchdogsController#user_log-instance_method

* Show User's Active Sessions

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Log-Getactivesessions

Code documentation Link: http://0.0.0.0:8808/docs/LoginsController#active_sessions-instance_method

* Upload National ID Verification Documents

Code documentation Link: http://0.0.0.0:8808/docs/NationalidVerificationsController#create-instance_method

* Upload Address Verification Documents

Code documentation Link: http://0.0.0.0:8808/docs/AddressVerificationsController#create-instance_method

* Upload Selfie Verification

Code documentation Link: http://0.0.0.0:8808/docs/SelfieVerificationsController#create-instance_method

* Show Country Details

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-GetCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#show-instance_method

* List All Countries

API documentation link:  https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-GetCountries

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#index-instance_method

* Sign Out

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/SessionsController#destroy-instance_method

# Test Code

* Create User
* Authenticate User
* list all users
* Create New Country
* List all Countries
* Show country Details
* Edit Country Details