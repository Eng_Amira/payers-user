# app/services/email_service.rb

class EmailService

  attr_accessor :user_mail, :subject , :text , :via , :email_provider
  require 'mailgun'

  # GET Email params
  # @param [Integer] user_mail The receiver user email address.
  # @param [String] subject The subject of the email.
  # @param [String] text The email content text.
  # @param [String] via the sender email address(payers@payers.net).
  # @param [String] EmailService_Provider The Email provider name.
  def initialize(user_mail:'client@payers.com', subject:'Welcome To Payers', via:'payers@payers.net', token:'token', mailer:'user_mailer', type:'default', email_provider:'mailgun', id:'id')
    
    @user_mail = user_mail
    @subject = subject
    @token = token
    @via = via
    @type = type
    @mailer = mailer
    @Email_Provider = email_provider
    @id = id

  end

  # Call a specific Email Provider and a specific function accourding to configuration
  def call

    if @Email_Provider == 'mailgun'
      EmailMailgun.new(user_mail:@user_mail, subject:@subject, via:@via, token:@token, id:@id, mailer:@mailer, type:@type).send_email
    end

  end

end