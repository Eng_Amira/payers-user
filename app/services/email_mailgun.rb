# app/services/email_mailgun.rb

class EmailMailgun

    attr_accessor :user_mail, :subject , :text , :via
    require 'mailgun'

    # GET Email params
    # @param [Integer] user_mail The receiver user email address.
    # @param [String] subject The subject of the email.
    # @param [String] text The email content text.
    # @param [String] via the sender email address(payers@payers.net).
    def initialize(user_mail:'client@payers.com',subject:'Welcome To Payers',via:'payers@payers.net',token:'Welcome', mailer:'user_mailer',type:'default', id:'id')
      
      @user_mail = user_mail
      @subject = subject
      @mailer = mailer
      @type = type
      @from = via
      @params = {:token => token,:mail => user_mail, :id => id }

    end

    # send text email through mailgun
    def send_email

        ac = ActionController::Base.new()   
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'

        
        message_params = {:from    => 'payers@payers.net',
                          :to      => @user_mail,
                          :subject => @subject,
                          :text    => @text,
                          :html    =>  ac.render_to_string("#{@mailer}/#{@type}.html", locals:  { :params => @params })}
        mg_client.send_message 'clients.payers.net', message_params
      
    end
#{self.class.to_s.underscore}/#{action_name}
end