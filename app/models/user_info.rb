class UserInfo < ApplicationRecord
    belongs_to :user ,:foreign_key => "user_id"
    belongs_to :selfie_verification ,:foreign_key => "selfie_verification_id", optional: true
    belongs_to :nationalid_verification ,:foreign_key => "nationalid_verification_id", optional: true
    belongs_to :address_verification ,:foreign_key => "address_verification_id", optional: true

    enum status: { UnVerified: 0, Verified: 1 }
end
