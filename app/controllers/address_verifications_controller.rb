class AddressVerificationsController < ApplicationController
  before_action :set_address_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login


  # show details of Address Verification of specific user
  # @param [Integer] id 
  # @return [String] UserName
  # @return [String] Country
  # @return [String] City
  # @return [String] State
  # @return [String] Street
  # @return [String] apartment_number
  # @return [String] Status
  # @return [String] Note 
  # @return [String] attachment 
  
  def show
    if current_user.id != @address_verification.user_id.to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new Address Verification
  # each user can create only one address verification
  def new
    @current_user_verification = AddressVerification.where("user_id =?", current_user.id.to_i).first
    if @current_user_verification != nil
      redirect_to root_path , notice: "not allowed"
    else
      @address_verification = AddressVerification.new
    end
  end

  # edit Address Verification
  # user can edit his address verification only if status is not verified
  # @param [Integer] id
  def edit
    if (@address_verification.status == "Verified") or (current_user.id != @address_verification.user_id.to_i)
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new Address Verification
  # @param [Integer] country_id
  # @param [String] city
  # @param [String] state
  # @param [String] street
  # @param [String] apartment_number
  # @param [Blob] attachment
  # @return [Integer] id
  # @return [String] username
  # @return [Integer] country_id
  # @return [String] state
  # @return [String] street
  # @return [String] apartment_number
  # @return [Blob] attachment
  # @return [Integer] status
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    @address_verification = AddressVerification.new(address_verification_params)
    @address_verification.user_id = current_user.id
    respond_to do |format|
      if @address_verification.save
        format.html { redirect_to @address_verification, notice: 'Address verification was successfully created.' }
        format.json { render :show, status: :created, location: @address_verification }
      else
        format.html { render :new }
        format.json { render json: @address_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  # edit Address Verification,
  # only admins can edit status and note.
  # @param [Integer] country_id
  # @param [String] city
  # @param [String] state
  # @param [String] street
  # @param [String] apartment_number
  # @param [Blob] attachment
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [Integer] country_id
  # @return [String] state
  # @return [String] street
  # @return [String] apartment_number
  # @return [Blob] attachment
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    respond_to do |format|
      if @address_verification.update(address_verification_params)
        format.html { redirect_to @address_verification, notice: 'Address verification was successfully updated.' }
        format.json { render :show, status: :ok, location: @address_verification }
      else
        format.html { render :edit }
        format.json { render json: @address_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address_verification
      @address_verification = AddressVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_verification_params
      params.require(:address_verification).permit(:user_id, :country_id, :city, :state, :street, :apartment_number, :status, :note, :attachment)
    end
end
