class CountriesController < ApplicationController
  before_action :set_country, only: [:show, :edit, :update, :destroy]
  before_action :require_login, except: [:index]
  # show list of all countries,
  # return with details of all countries.
  # @return [String] short_code
  # @return [String] Full_Name
  # @return [String] Currency
  def index
    @countries = Country.all
  end

  # show details of the country
  # @param [Integer] id 
  # @return [String] short_code
  # @return [String] Full_Name
  # @return [String] Currency
  # @return [String] Phone_code
  # @return [String] language
  # @return [Integer] active
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def country_params
      params.require(:country).permit(:short_code, :Full_Name, :Phone_code, :Currency, :language, :active)
    end
end
