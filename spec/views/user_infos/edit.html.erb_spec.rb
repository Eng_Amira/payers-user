require 'rails_helper'

RSpec.describe "user_infos/edit", type: :view do
  before(:each) do
    @user_info = assign(:user_info, UserInfo.create!(
      :user_id => 1,
      :mobile => "MyString",
      :address_verification_id => 1,
      :nationalid_verification_id => 1,
      :nationalid_verification_id => 1,
      :selfie_verification => 1,
      :status => 1
    ))
  end

  it "renders the edit user_info form" do
    render

    assert_select "form[action=?][method=?]", user_info_path(@user_info), "post" do

      assert_select "input[name=?]", "user_info[user_id]"

      assert_select "input[name=?]", "user_info[mobile]"

      assert_select "input[name=?]", "user_info[address_verification_id]"

      assert_select "input[name=?]", "user_info[nationalid_verification_id]"

      assert_select "input[name=?]", "user_info[nationalid_verification_id]"

      assert_select "input[name=?]", "user_info[selfie_verification]"

      assert_select "input[name=?]", "user_info[status]"
    end
  end
end
