require 'test_helper'

class AffilateProgramsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @affilate_program = affilate_programs(:one)
  end

  test "should get index" do
    get affilate_programs_url
    assert_response :success
  end

  test "should get new" do
    get new_affilate_program_url
    assert_response :success
  end

  test "should create affilate_program" do
    assert_difference('AffilateProgram.count') do
      post affilate_programs_url, params: { affilate_program: { refered_by: @affilate_program.refered_by, user_id: @affilate_program.user_id } }
    end

    assert_redirected_to affilate_program_url(AffilateProgram.last)
  end

  test "should show affilate_program" do
    get affilate_program_url(@affilate_program)
    assert_response :success
  end

  test "should get edit" do
    get edit_affilate_program_url(@affilate_program)
    assert_response :success
  end

  test "should update affilate_program" do
    patch affilate_program_url(@affilate_program), params: { affilate_program: { refered_by: @affilate_program.refered_by, user_id: @affilate_program.user_id } }
    assert_redirected_to affilate_program_url(@affilate_program)
  end

  test "should destroy affilate_program" do
    assert_difference('AffilateProgram.count', -1) do
      delete affilate_program_url(@affilate_program)
    end

    assert_redirected_to affilate_programs_url
  end
end
