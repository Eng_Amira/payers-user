class CreateVerifications < ActiveRecord::Migration[5.2]
  def change
    create_table :verifications do |t|
      t.integer :admin_id
      t.text :email_confirmation_token
      t.datetime :email_confirmed_at

      t.timestamps
    end
  end
end
